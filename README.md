# Snake 2.0

### Roberto Martins da Nóbrega Mat: 14/0065547

## Versão do pacote Java utilizado

java-11-openjdk-amd64

# Como Jogar

Exiestem duas maneiras de executar o jogo a primeira seria abrir o terminal na pasta ep2 onde está localizado o arquivo .jar de nome kingkobra.jar e escrever o seguinte comando "java -jar kingkobra.jar" sem as aspas, o segundo seria importando a pasta ep2 para a sua IDE preferida(recomendo Eclipse), compile e execute.


# EP2 - OO 2019.2 (UnB - Gama)

Turma do Renato
Data de entrega: 12/11/2019

## Como Jogar

1. As setas do teclado movimentam a cobra, podendo se mover para cima, baixo, esquerda e direita apenas.
2. Existem 3 tipos de cobras, "normal" já conhecida por todos a qual não possui nenhum poder especial, a cobra "kitty" que consegue passar por cima das barreiras do mapa mas como as demais continua morrendo se passar pelas bordas e por fim a "star" que ganha o dobro de pontos e 2 unidades de tamanho ao comer.
3. Existem ainda 4 tipos de "comidas" a simples que dá 1 ponto e cresce o tamanho em 1, a grande que da o dobro de pontos, a Maçã que reduz o tamanho para o tamanho inicial sem que haja a perda de pontos e a bomba que te mata caso você a coma, mas cuidado todas elas podem aparecer no seu caminho a qualquer momento.
4. O jogo só acaba com o game over que exibirá sua potuação.
