package game;

//import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import game.gfx.Assets;
import game.input.KeyManager;
import game.states.GameState;
import game.states.MenuState;
import game.states.State;
import game.display.Display;

public class Game implements Runnable {
	
	private Display display;
	private String select;
	public int width, height;
	public String title;
	private boolean running = false;
	private Thread thread;
	
	private BufferStrategy buff;
	private Graphics graph;
	
	//states
	private State gameState;
	private State menuState;
	
	//input
	private KeyManager keyManager;
	
	public Game(String title, int width, int height,String select) {
		this.select=select;
		this.width = width;
		this.height = height;
		this.title = title;
		keyManager = new KeyManager();
	}
	
	

	private void init() {
		display = new Display(title, width, height);
		display.getFrame().addKeyListener(keyManager);
		Assets.init();
		
		gameState = new GameState(this,select);
		menuState = new MenuState(this);
		State.setState(gameState);
		//State.setState(menuState);
	}
	
	private void tick() {
		keyManager.tick();
		
		if(State.getState()!= null)
			State.getState().tick();
		
	}
	private void render() {
		buff = display.getCanvas().getBufferStrategy();
		if (buff == null) {
			display.getCanvas().createBufferStrategy(3);
			return;
		}
		graph = buff.getDrawGraphics();
		//clear screen
		graph.clearRect(0, 0, width, height);
		//draw
		
		if(State.getState()!= null) 
			State.getState().render(graph);
			
		
		//graph.drawImage(Assets.ratelGameover, 0, 0, null);

		
		//end
		buff.show();
		graph.dispose();
		
	}
	
	public void run() {
		init();
		
		int fps = 10;
		double timePerTick = 1000000000 / fps;
		double delta = 0;
		long now;
		long lastTime = System.nanoTime();
		long timer = 0;
		int ticks = 0;
		
		while(running) {
			now = System.nanoTime();
			delta += (now - lastTime) / timePerTick;
			timer += now - lastTime;
			lastTime = now;
			if(delta>=1) {
				tick();
				render();
				ticks++;
				delta--;
				
			}
			if (timer >= 1000000000) {
				//System.out.println("Frames:" + ticks);
				ticks = 0;
				timer = 0;
			}
			
		}
		stop();
		
		
	}
	
	public KeyManager getKeyManager() {
		return keyManager;
	}
	public synchronized void start() {
		if (running)
			return;
		running = true;
		thread = new Thread(this);
		thread.start();
		
	}
	public synchronized void stop() {
		if (!running)
			return;
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

}
