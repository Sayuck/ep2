package game.states;

import java.awt.Graphics;

import game.Game;
import game.entities.creatures.KittySnake;
import game.entities.creatures.NormalSnake;
import game.entities.creatures.Snake;
import game.entities.creatures.StarSnake;
import game.gfx.Assets;

public class GameState extends State {
	
	private Snake snake;
	private String serpent = "star";
	
	public GameState(Game game,String s) {
		super(game);
		this.serpent = s;
		switch (serpent) {
		case "normal":
		snake = new NormalSnake(game, 100,0);
		break;
		case "kitty":
			snake = new KittySnake(game, 100,0);
			break;
		case "star":
			snake = new StarSnake(game, 100,0);
			break;
				
		}
		
	}

	@Override
	public void tick() {
		snake.tick();
		
	}

	@Override
	public void render(Graphics graph) {
		//graph.drawImage(Assets.border,0,0,330,330, null);
		graph.drawImage(Assets.background, 0, 0, 300, 300, null);
		snake.render(graph);
		
	}
	
	

}
