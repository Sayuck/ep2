package game.entities.creatures;

import java.awt.Graphics;

import game.Game;
import game.gfx.Assets;

public abstract class Foods extends Creature{
	
	protected int fruitX, fruitY;
	private final int RAND_POS = 29;
	private Game game;
	protected Snake snake;

	public Foods(Snake snake, Game game, float x, float y) {
		super(x, y, Creature.DEFAULT_CREATURE_WIDTH, Creature.DEFAULT_CREATURE_HEIGHT);
		this.game = game;
		this.fruitX = (int)x;
		this.fruitY = (int)y;
		this.snake = snake;
		//locateFood();
	}
	
    public int getFruitX() {
		return fruitX;
	}

	public void setFruitX(int fruitX) {
		this.fruitX = fruitX;
	}

	public int getFruitY() {
		return fruitY;
	}

	public void setFruitY(int fruitY) {
		this.fruitY = fruitY;
	}

	public void locateFood() {

        int r = (int) (Math.random() * RAND_POS);
        fruitX = ((r * this.height));

        r = (int) (Math.random() * RAND_POS);
        fruitY = ((r * this.width));
    }
    public void checkFood() {

        if ((snake.getHeadX() == fruitX) && (snake.getHeadY() == fruitY)) {

            snake.setBody(1);
            snake.setPoints(1);
            setFruitX(-10);
            setFruitX(-10);
            //locateFood();
        }
    }

	@Override
	public void tick() {

		
	}

	@Override
	public void render(Graphics graph) {
		graph.drawImage(Assets.rat, (int)getFruitX(), (int)getFruitY(), width, height, null);
		
	}

}
