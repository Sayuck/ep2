package game.entities.creatures;

import java.awt.Graphics;

import game.Game;
import game.gfx.Assets;

public class FoodX extends Foods{

	public FoodX(Snake snake, Game game, float x, float y) {
		super(snake, game, x, y);
		
	}
	@Override
    public void checkFood() {

        if ((snake.getHeadX() == fruitX) && (snake.getHeadY() == fruitY)) {

            snake.setBodyToOne();
            setFruitX(-10);
            setFruitY(-10);
            //locateFood();
        }
    }
	
	@Override
	public void render(Graphics graph) {
		graph.drawImage(Assets.decreasefood, (int)getFruitX(), (int)getFruitY(), width, height, null);
		
	}
}
