package game.entities.creatures;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;


import game.Game;
import game.gfx.Assets;

public abstract class Snake extends Creature {
	
	private final int ALL_DOTS = 900;
    private final int RAND_POS = 29;
    protected int points = 0;


	private Game game;
	protected int body = 1;
	protected int fruitX = 100;
	protected int fruitY = 100;
	protected int outfruit = -50;
	protected boolean inGame = true;
	protected int countToNormalFood = 0, countToBomb = 0, countToBigFood= 0,countToXfood = 0;
	protected String head = "right";
	protected final float lastX[] = new float[ALL_DOTS];
	protected final float lastY[] = new float[ALL_DOTS];
	protected Foods foodnormal;
	protected Foods foodBomb;
	protected Foods foodX;
	protected Foods foodBig;
	protected Wall wall;
	protected float headX,headY;
	
	
	
	
	//getters and setters
	
	public void setInGame(boolean inGame) {
		this.inGame = inGame;
	}

	public void setBody(int num) {
		this.body = (body+num);
	}
	
	public float getHeadX() {
		headX = lastX[0];
		return headX;
	}

	public void setHeadX(float headX) {
		this.headX = headX;
	}

	public float getHeadY() {
		headY = lastY[0];
		return headY;
	}

	public void setHeadY(float headY) {
		this.headY = headY;
	}

	public void setBodyToOne() {
		this.body = 1;
	}
	
	public int getPoints() {
		return points;
	}

	public void setPoints(int num) {
		this.points = (points+num);
	}
	
	
	public Snake(Game game, float x, float y) {
		super(x, y, Creature.DEFAULT_CREATURE_WIDTH, Creature.DEFAULT_CREATURE_HEIGHT);
		this.game = game;
		this.lastX[0] = x;
		this.lastY[0] = y;
		foodnormal = new FoodNormal(this, game, fruitX, fruitY);
		foodBomb = new FoodBomb(this, game, outfruit, outfruit);
		foodX = new FoodX(this, game, outfruit, outfruit);
		foodBig = new FoodBig(this, game, outfruit, outfruit);
		wall = new Wall(this,game,outfruit, outfruit);
	}
	

	@Override
	public void tick() {
		
		if(inGame) {
			checkCollision();
			foodnormal.checkFood();
			foodBig.checkFood();
			foodBomb.checkFood();
			foodX.checkFood();
			wall.checkColisionToWall();
			checkCollision();
			snakeHeadMovement();
			checkCollision();
			wall.checkColisionToWall();
			
			if (countToNormalFood == 60) {
				foodnormal.locateFood();
				countToNormalFood = 0;
			}else {
				countToNormalFood++;
			}
			
			if(countToBomb == 200) {
				foodBomb.locateFood();
				countToBomb = 0;
				
			}else {				
				countToBomb++;
			}
			
			if(countToXfood == 800) {
				foodX.locateFood();
				countToXfood =0;
				
			}else {				
				countToXfood++;	
			}
			
			if(countToBigFood == 400) {
				foodBig.locateFood();
				countToBigFood = 0;
				
			}else {				
				countToBigFood++;
				
			}
	
		}
	}

	@Override
	public void render(Graphics graph) {

		if(inGame) {
			wall.render(graph);
			foodnormal.render(graph);
			foodBomb.render(graph);
			foodX.render(graph);
			foodBig.render(graph);
			
		if (head == "up" ) {
	
			graph.drawImage(Assets.uphead, (int) lastX[0], (int) lastY[0], width, height, null);
		}
		else if (head == "down") {
	
			graph.drawImage(Assets.downhead, (int) lastX[0], (int) lastY[0], width, height, null);
		}
		else if (head == "right") {
		
			graph.drawImage(Assets.righthead,(int) lastX[0], (int) lastY[0], width, height, null);
		}
		else if (head == "left") {

			graph.drawImage(Assets.lefthead, (int) lastX[0], (int) lastY[0], width, height, null);
		}
		for (int b = body; b > 1; b--) {	
			graph.drawImage(Assets.body, (int) lastX[b], (int) lastY[b], width, height, null);
			}
		Toolkit.getDefaultToolkit().sync();
		}
		
		else {
			gameOver(graph);
		}

	}
	public void checkCollision() {

		if (lastY[0] > game.height || (lastY[0] < 0) || ((lastX[0]) > (game.width))|| (lastX[0] < 0) ) {
		inGame = false;
		}
		for (int b = body; b > 0; b--) {
			if ((b > 4) && (lastX[0] == lastX[b]) && (lastY[0] == lastY[b])) {
                inGame = false;
            }
		}

//		if (!inGame) {
//        timer.stop();
//		}
}
	
	public void bodyMovement() {
		for (int b = body; b > 0; b--) {	
			lastX[b] = lastX[(b - 1)];
            lastY[b] = lastY[(b - 1)];}
	}
	
	public void gameOver(Graphics graph) {
		graph.drawImage(Assets.gameOver, 0, 0, game.width, game.height, null);
		Font normal = new Font("Helvetica", Font.BOLD, 22);
		graph.setColor(Color.RED);
		graph.setFont(normal);
		graph.drawString("Ponturação Final: "+points, 30, 20);
		
	}
	public void keyTest() {
		
		if (game.getKeyManager().down && game.getKeyManager().up) {
			game.getKeyManager().down = true; 
			game.getKeyManager().up = false;
			//checkCollision();
		}
		if (game.getKeyManager().down && game.getKeyManager().left) {
			game.getKeyManager().down = true;
			game.getKeyManager().left = false;
			//checkCollision();
			}
		if (game.getKeyManager().down && game.getKeyManager().right) {
			game.getKeyManager().down = true;
			game.getKeyManager().right = false;
			//checkCollision();
		}
		if(game.getKeyManager().up && game.getKeyManager().left) {
			game.getKeyManager().up = true;
			game.getKeyManager().left= false;
			//checkCollision();
		}
		if(game.getKeyManager().up && game.getKeyManager().right) {
			game.getKeyManager().up = true;
			game.getKeyManager().right = false;
			//checkCollision();
		}
		if(game.getKeyManager().left && game.getKeyManager().right) {
			game.getKeyManager().left = true;
			game.getKeyManager().right = false;
			//checkCollision();
		}
	}
	
	public void snakeHeadMovement() {
		
		
		if (!game.getKeyManager().down || !game.getKeyManager().up ||!game.getKeyManager().right ||!game.getKeyManager().left) {
			
			if (head == "up") 
				lastY[0] -= 10;		
			else if (head == "down") 
				lastY[0] += 10;
			else if (head == "left")
				lastX[0] -= 10;
			else if (head == "right")
				lastX[0] += 10;
			//checkCollision();
			bodyMovement();
		}
		keyTest();
		if(game.getKeyManager().up && head!= "up" && head!= "down") {
			head = "up";
			lastY[0] -= 10;
			//checkCollision();
			bodyMovement();
		}
		if(game.getKeyManager().down && head != "down" && head!= "up") {
			head = "down";
			lastY[0] += 10;
			//checkCollision();
			bodyMovement();
		}
		if(game.getKeyManager().left && head != "left" && head != "right") {
			head = "left";
			lastX[0] -= 10;
			//checkCollision();
			bodyMovement();

		}
		if(game.getKeyManager().right && head != "right" && head != "left") {
			head = "right";
			lastX[0] += 10;
			//checkCollision();
			bodyMovement();
		}
		
	}

}
