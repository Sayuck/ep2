package game.entities.creatures;

import java.awt.Graphics;

import game.Game;
import game.gfx.Assets;

public class Wall extends Creature{

	private float x[] = new float[40];
	private float y[] = new float[40];
	private Snake snake;
	private Game game;
	private int pos = 0;
	

	public Wall(Snake snake, Game game,int xini, int yini) {
		super(xini,yini,Creature.DEFAULT_CREATURE_WIDTH,Creature.DEFAULT_CREATURE_HEIGHT);
		this.x[0] = xini;
		this.y[0] = yini;
		this.snake = snake;
		this.game = game;
		wallCreate();
	}
	public void wallCreate(){
		for (int i = 0; i< 10; i++) {
			x[i] = pos;
			pos+=10;
			y[i] = 80;
		}
		pos = 200;
		for (int i = 10; i< 20; i++) {
			x[i] = pos;
			pos+=10;
			y[i] = 150;
		}
		
		
	}

	public void checkColisionToWall() {
		for (int i = 0; i<20; i++) {
		if ( (int)snake.getHeadX() == x[i] && (int)snake.getHeadY() == y[i] ) 
			snake.setInGame(false);	
		}
			
		
	}
	@Override
	public void render(Graphics graph) {
		
		for (int i = 0; i< 20; i++) {
			graph.drawImage(Assets.border,(int) x[i],(int) y[i],width, height, null);
		}
		
	}

	@Override
	public void tick() {
		// TODO Auto-generated method stub
		
	}

}
