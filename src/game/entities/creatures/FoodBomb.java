package game.entities.creatures;

import java.awt.Graphics;

import game.Game;
import game.gfx.Assets;

public class FoodBomb extends Foods{

	public FoodBomb(Snake snake, Game game, float x, float y) {
		super(snake, game, x, y);
		
	}
	@Override
    public void checkFood() {

        if ((snake.getHeadX() == fruitX) && (snake.getHeadY() == fruitY)) {

        	snake.setInGame(false);
            setFruitX(-10);
            setFruitX(-10);}
	}
	@Override
	public void render(Graphics graph) {
		graph.drawImage(Assets.bomb, (int)getFruitX(), (int)getFruitY(), width, height, null);
		
	}
}
