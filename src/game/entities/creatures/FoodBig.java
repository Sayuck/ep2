package game.entities.creatures;

import java.awt.Graphics;

import game.Game;
import game.gfx.Assets;

public class FoodBig extends Foods{

	public FoodBig(Snake snake, Game game, float x, float y) {
		super(snake, game, x, y);
		
	}
	@Override
    public void checkFood() {

        if ((snake.getHeadX() == fruitX) && (snake.getHeadY() == fruitY)) {

            snake.setBody(1);
            snake.setPoints(2);
            setFruitX(-10);
            setFruitX(-10);
            //locateFood();
        }
    }
	
	@Override
	public void render(Graphics graph) {
		graph.drawImage(Assets.bigfood, (int)getFruitX(), (int)getFruitY(), width, height, null);
		
	}

}
