package game.entities.creatures;

import java.awt.Graphics;
import java.awt.Toolkit;

import game.Game;
import game.gfx.Assets;

public class KittySnake extends Snake{

	public KittySnake(Game game, float x, float y) {
		super(game, x, y);
	}

	@Override
	public void tick() {
		
		if(inGame) {
			checkCollision();
			foodnormal.checkFood();
			foodBig.checkFood();
			foodBomb.checkFood();
			foodX.checkFood();
			checkCollision();
			snakeHeadMovement();
			checkCollision();
			
			if (countToNormalFood == 50) {
				foodnormal.locateFood();
				countToNormalFood = 0;
			}else {
				countToNormalFood++;
			}
			
			if(countToBomb == 400) {
				foodBomb.locateFood();
				countToBomb = 0;
				
			}else {				
				countToBomb++;
			}
			
			if(countToXfood == 800) {
				foodX.locateFood();
				countToXfood =0;
				
			}else {				
				countToXfood++;	
			}
			
			if(countToBigFood == 400) {
				foodBig.locateFood();
				countToBigFood = 0;
				
			}else {				
				countToBigFood++;
				
			}
	
		}
	}
	
		@Override
		 public void render(Graphics graph) {

			if(inGame) {
				wall.render(graph);
				foodnormal.render(graph);
				foodBomb.render(graph);
				foodX.render(graph);
				foodBig.render(graph);
				
			if (head == "up" ) {
		
				graph.drawImage(Assets.uphead, (int) lastX[0], (int) lastY[0], width, height, null);
			}
			else if (head == "down") {
		
				graph.drawImage(Assets.downhead, (int) lastX[0], (int) lastY[0], width, height, null);
			}
			else if (head == "right") {
			
				graph.drawImage(Assets.righthead,(int) lastX[0], (int) lastY[0], width, height, null);
			}
			else if (head == "left") {

				graph.drawImage(Assets.lefthead, (int) lastX[0], (int) lastY[0], width, height, null);
			}
			for (int b = body; b > 1; b--) {	
				graph.drawImage(Assets.kitty, (int) lastX[b], (int) lastY[b], width, height, null);
				}
			Toolkit.getDefaultToolkit().sync();
			}
			
			else {
				gameOver(graph);
			}
		}
	}


