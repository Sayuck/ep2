package game.entities.creatures;

import java.awt.Graphics;
import java.awt.Toolkit;

import game.Game;
import game.gfx.Assets;

public class StarSnake extends Snake{

	public StarSnake(Game game, float x, float y) {
		super(game, x, y);
		
	}
	@Override
	public void setBody(int num) {
		this.body = body + num+1;
	}
	@Override
	public void setPoints(int num) {
		this.points = points + num+1;
	}

	@Override
	public void render(Graphics graph) {

		if(inGame) {
			wall.render(graph);
			foodnormal.render(graph);
			foodBomb.render(graph);
			foodX.render(graph);
			foodBig.render(graph);
			//graph.drawImage(Assets.rat, fruitX, fruitY, width, height, null);
			
		if (head == "up" ) {
	
			graph.drawImage(Assets.uphead, (int) lastX[0], (int) lastY[0], width, height, null);
		}
		else if (head == "down") {
	
			graph.drawImage(Assets.downhead, (int) lastX[0], (int) lastY[0], width, height, null);
		}
		else if (head == "right") {
		
			graph.drawImage(Assets.righthead,(int) lastX[0], (int) lastY[0], width, height, null);
		}
		else if (head == "left") {

			graph.drawImage(Assets.lefthead, (int) lastX[0], (int) lastY[0], width, height, null);
		}
		for (int b = body; b > 1; b--) {	
			graph.drawImage(Assets.star, (int) lastX[b], (int) lastY[b], width, height, null);
			}
		Toolkit.getDefaultToolkit().sync();
		}
		
		else {
			gameOver(graph);
		}
	}

}
