package game.gfx;

import java.awt.image.BufferedImage;

public class Assets {
	public static BufferedImage body,border, uphead, downhead, lefthead, righthead, rat, ratelGameover, ratelSprite, 
	gameOver, bigfood, bomb, background, kitty, star,decreasefood;  
	
	public static void init() {
		
		ratelGameover = ImageLoader.loadImage("/textures/ratelecobra.jpg");
		background = ImageLoader.loadImage("/textures/background2.jpg");
		uphead = ImageLoader.loadImage("/textures/upmouth.png");
		downhead = ImageLoader.loadImage("/textures/downmouth.png");
		lefthead = ImageLoader.loadImage("/textures/leftmouth.png");
		righthead = ImageLoader.loadImage("/textures/rightmouth.png");
		border = ImageLoader.loadImage("/textures/ground.jpeg");
		body = ImageLoader.loadImage("/textures/snakeimage.png");
		gameOver = ImageLoader.loadImage("/textures/gameover.jpg");
		rat = ImageLoader.loadImage("/textures/enemy.png");
		kitty = ImageLoader.loadImage("/textures/kittybody.png");
		star = ImageLoader.loadImage("/textures/starbody.png");
		bomb = ImageLoader.loadImage("/textures/bomb.png");
		bigfood = ImageLoader.loadImage("/textures/foodX.png");
		decreasefood = ImageLoader.loadImage("/textures/morango.png");
		
	}

}
