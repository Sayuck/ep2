package game.display;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import game.Game;



public class MenuGame implements ActionListener{
	private JFrame tela;
	private JButton btnKitty;
	private JButton btnSimple;
	private JButton btnStar;
	private JPanel panel;
	private Game game;
	
	
	public MenuGame(){
		tela = new JFrame();
		tela.setTitle("KingKobra");
		tela.setLocationRelativeTo(null);
		tela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		tela.setPreferredSize(new Dimension(300,300));
		tela.pack();
		panel = new JPanel();
		
		btnKitty = new JButton("KITTY");
		btnKitty.addActionListener(this);
		btnKitty.setActionCommand("KITTY");
		btnStar = new JButton("STAR");
		btnStar.addActionListener(this);
		btnStar.setActionCommand("STAR");
		btnSimple = new JButton("SIMPLE");
		btnSimple.addActionListener(this);
		btnSimple.setActionCommand("SIMPLE");
		panel.add(btnKitty);
		panel.add(btnSimple);
		panel.add(btnStar);
		tela.add(panel);
		tela.setVisible(true);
		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()) {
			case "KITTY":
				game = new Game("KingKobra", 300,300,"kitty");
				game.start();
				tela.setVisible(false);
				tela.dispose();
				break;
			case "STAR":
				game = new Game("KingKobra", 300,300,"star");
				game.start();
				tela.setVisible(false);
				tela.dispose();
				break;
			case "SIMPLE":
				game = new Game("KingKobra", 300,300,"normal");
				game.start();
				tela.setVisible(false);
				tela.dispose();
				break;
				
			
		}
		
	}

}
